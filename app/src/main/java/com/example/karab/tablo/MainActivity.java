package com.example.karab.tablo;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Integer counterS = 0;
    private Integer counterZ = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @SuppressLint("SetTextI18n")
    public void onClickBtnGol(View view) {

        switch (view.getId()) {
            case R.id.button2: {
                counterS++;
                TextView counterViev = findViewById(R.id.counterOne);
                counterViev.setText(counterS.toString());
                break;
            }
            case R.id.button3: {
                counterZ++;
                TextView counterViev =  findViewById(R.id.counterTwo);
                counterViev.setText(counterZ.toString());
                break;
            }
            case R.id.button: {
                counterZ = 0;
                counterS = 0;
                TextView counterVievs =  findViewById(R.id.counterOne);
                counterVievs.setText(counterS.toString());
                TextView counterViev =  findViewById(R.id.counterTwo);
                counterViev.setText(counterZ.toString());
                break;
            }
        }
    }
        @Override
        protected void onStart() {
            super.onStart();
        }

        @Override
        protected void onStop() {
            super.onStop();
        }
    @Override
    protected void onResume() {
        super.onResume();
        resetUI();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onRestart() {
        super.onRestart();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @SuppressLint("SetTextI18n")
    private void resetUI() {
        ((TextView) findViewById(R.id.counterOne)).setText(counterS.toString());
        ((TextView) findViewById(R.id.counterTwo)).setText(counterZ.toString());


    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState!=null  && ((savedInstanceState.containsKey("counterS"))&& (savedInstanceState.containsKey("counterZ")))) {
            counterS=savedInstanceState.getInt("counterS");
            counterZ=savedInstanceState.getInt("counterZ");
        }}
        @Override
        protected void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putInt("counterS",counterS);
            outState.putInt("counterZ",counterZ);

        }


    }


